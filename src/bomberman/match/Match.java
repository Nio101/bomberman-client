package bomberman.match;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.activation.ActivationException;
import bomberman.client.MSClient;


/** Server attivabile che gestisce una partita**/

public interface Match extends Remote {
	int join(MSClient userStub, String username) throws ActivationException, IOException, ClassNotFoundException;
	void move(int uid, int x, int y) throws ActivationException, IOException, ClassNotFoundException;
	void fire(int uid, int x, int y) throws ActivationException, IOException, ClassNotFoundException;
	int [][] getMap() throws ActivationException, IOException, ClassNotFoundException;
}
