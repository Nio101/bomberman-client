package bomberman.client;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public interface MSClient extends Remote { 
	void export() throws RemoteException;
	void exec() throws RemoteException;
	void userJoins(int uid) throws RemoteException;
    void userLeaves(int uid) throws RemoteException;
}
