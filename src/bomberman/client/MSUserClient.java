package bomberman.client;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public interface MSUserClient extends MSClient{
	void move(int uid, int x, int y) throws RemoteException;
    void fire(int uid, int x, int y) throws RemoteException;
}
