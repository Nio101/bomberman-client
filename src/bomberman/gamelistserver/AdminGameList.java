package bomberman.gamelistserver;

import java.rmi.RemoteException;
import bomberman.client.MSClient;


public interface AdminGameList extends GameList {
	public void createMatch(String mid) throws RemoteException;
	public void deleteMatch(String mid) throws RemoteException;
}