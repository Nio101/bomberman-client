package bomberman.gamelistserver;

import java.rmi.Remote;
import java.rmi.RemoteException;

import bomberman.match.Match;
import bomberman.client.*;


public interface GameList extends Remote {
    public String [] getMatchesList() throws RemoteException;
    public Match getMatch(String mid) throws RemoteException;
}

