package bomberman.login;

import java.rmi.Remote;
import java.rmi.RemoteException;
import bomberman.client.*;


public interface Bootstrap extends Remote  {
	Runnable getClient() throws RemoteException;
	MSClient login(String username, String password) throws RemoteException;
} 
