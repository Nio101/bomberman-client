package bomberman;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.rmi.PortableRemoteObject;
import java.rmi.RemoteException;
import java.util.Properties;
import bomberman.login.Bootstrap;
import java.io.*;
import java.rmi.server.*;


public class ClientIIOP {

	public static void main(String[] args){
		System.setSecurityManager(new SecurityManager());

		try {
			String serverAddress = args.length > 0 ? args[0] : "127.0.0.1";
			int cosServerPort = 1100;
			int httpServerPort = 8000;
						
			Properties pr = new Properties();
			pr.put("java.naming.factory.initial", "com.sun.jndi.cosnaming.CNCtxFactory");
			pr.put("java.naming.provider.url", "iiop://"+serverAddress+":"+cosServerPort);
			InitialContext ic = new InitialContext(pr);
			Object objRef = ic.lookup("AuthServer");

			Bootstrap  bootstrap = (Bootstrap)PortableRemoteObject.narrow(objRef, Bootstrap.class);
			Runnable client = bootstrap.getClient();
			client.run();			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
 
