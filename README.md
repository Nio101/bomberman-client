This is a university project for the network programming course. The purpose of the project was to program a bomberman clone using Java RMI. This is not meant to be used as a real game, it has been done to prove that I have learned the course topics. In this repository there is the client source code, for the server please visit [this link](https://bitbucket.org/Nio101/bomberman-server).

Main topics:

* Java RMI;
* CORBA;
* SSL socket;
* Naming services;
* Distributed Garbage Collection;
* Dynamic code downloading;
* Security mangament;
* Migration of mobile agent;
* Unicasts Servers;
* Activatables Servers.